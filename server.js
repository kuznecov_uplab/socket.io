"use strict";

const socketIo = require('socket.io');
const express = require('express');

const app = express();
const server = app.listen(3000, function () {
    console.log('Example app listening on port 3000!')
});
const io = socketIo(server);

let counter = 0;

io.on('connection', function (browserSocket) {
    io.emit('info', {
        connections: ++counter,
        info: (() => {
            const info = [];

            if (io.sockets && io.sockets.sockets){
                for (let s in io.sockets.sockets){
                    info.push(io.sockets.sockets[s].request.headers['user-agent'])
                }
            }

            return info;
        })()
    }, 1);

    browserSocket.on('from_mobile', throttle(100, function(data){
        io.emit('value', data);
    }, true));

    browserSocket.on('disconnect', function(){
        "use strict";

        io.emit('info', {
            connections: --counter
        }, 1);
    });
});

app.get('/route', function (req, res) {

});

app.use(express.static('www'));

function throttle(delay, fn, immediate) {
    var now = Date.now() - (immediate ? delay : 0);

    return function(){
        const from = Date.now();
        const interval = from - now;

        if (interval < delay) {
            return;
        }

        now = from;

        fn.apply(this, arguments);

        return this;
    };
}